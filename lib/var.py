# Globals

# Health Variables
global vphys
global vsocio
global vpsych
global vhealth
vhealth = (vphys + vsocio + vpsych)/100

# Personal Variables
global vplayer
vplayer = "Player Name"
global vclass
vclass = "Adventurer"
